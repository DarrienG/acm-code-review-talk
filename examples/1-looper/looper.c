#include <stdio.h>

int main(int argc, char *argv[]) {
  int counter = 0;
  for (int i = 0; i < 100000; ++i) {
    if (i % 1000 == 0) {
      counter += 1;
      printf("%d\n", counter);
    }
  }
}
